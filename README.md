
## Ports

```
-p 500:500/udp -p 4500:4500/udp -p 1701:1701/tcp for L2TP/IPSec
-p 1194:1194/udp for OpenVPN
-p 443:443/tcp for SoftEther over HTTPS
```

## Kubernetes deployment

```
wget https://bitbucket.org/paskalmaksim/softethervpn/get/master.tar.gz
tar -xvf master.tar.gz --strip-components=1
kubectl create -f deployment.yaml
```

## Environments
Create environments for running SoftEther or create file, and run docker with option `--env-file`

```
# /app/vpn-config
#
# IPsec Pre-Shared key - default random
# PSK=
# Hub name - default DEFAULT
# HUB=DEFAULT
# MTU - default 1500
# MTU=1500
# Hub password - default random
# HPW =
# Server password - default random
# SPW =
# User with name test1 and password 12345
VPN_USER_TEST1=12345
# User with name test2 and password 54321
VPN_USER_TEST2=54321
```

## Run with defaults certs

```
docker run -d --name softethervpn --restart=always \
   -v /var/log/vpnserver/packet_log:/usr/vpnserver/packet_log \
   -v /var/log/vpnserver/security_log:/usr/vpnserver/security_log \
   -v /var/log/vpnserver/server_log:/usr/vpnserver/server_log \
   --cap-add NET_ADMIN \
   -p 500:500/udp \
   -p 4500:4500/udp \
   -p 1701:1701/tcp \
   -p 1194:1194/udp \
   -p 443:443/tcp \
   --env-file /app/vpn-config \
   paskalmaksim/softethervpn
```

run with one user

```
PSK=$(cat /dev/urandom | tr -dc 'A-Za-z0-9' | fold -w 20 | head -n 1) &&
VPN_USER_GUEST=$(cat /dev/urandom | tr -dc 'A-Za-z0-9' | fold -w 20 | head -n 1) &&
docker run -d --name softethervpn --restart=always \
   -v /var/log/vpnserver/packet_log:/usr/vpnserver/packet_log \
   -v /var/log/vpnserver/security_log:/usr/vpnserver/security_log \
   -v /var/log/vpnserver/server_log:/usr/vpnserver/server_log \
   --cap-add NET_ADMIN \
   -p 500:500/udp \
   -p 4500:4500/udp \
   -p 1701:1701/tcp \
   -p 1194:1194/udp \
   -p 443:443/tcp \
   -e PSK=$PSK \
   -e VPN_USER_GUEST=$VPN_USER_GUEST \
   paskalmaksim/softethervpn &&
echo shared password = $PSK &&
echo password for guest = $VPN_USER_GUEST
```


## Generate certs
For certificate persistence for OpenVPN - you need to genenerate it for example in folder `/app/certs`

```
docker run -it -v /app/certs:/certs paskalmaksim/softethervpn sh -c generate
```
and then run with certs

```
docker run -d --name softethervpn --restart=always \
   -v /app/certs:/certs \
   -v /var/log/vpnserver/packet_log:/usr/vpnserver/packet_log \
   -v /var/log/vpnserver/security_log:/usr/vpnserver/security_log \
   -v /var/log/vpnserver/server_log:/usr/vpnserver/server_log \
   --cap-add NET_ADMIN \
   -p 500:500/udp \
   -p 4500:4500/udp \
   -p 1701:1701/tcp \
   -p 1194:1194/udp \
   -p 443:443/tcp \
   --env-file /app/vpn-config \
   paskalmaksim/softethervpn
```
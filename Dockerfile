FROM alpine:3.8

ADD https://github.com/SoftEtherVPN/SoftEtherVPN_Stable/releases/download/v4.28-9669-beta/softether-src-v4.28-9669-beta.tar.gz /build/softether.tar.gz

COPY ./scripts /usr/local/bin/

RUN cd /build \
  && tar -xf softether.tar.gz --strip=1 \
  && apk add --no-cache --virtual .build-deps \
    build-base \
    ncurses-dev \
    openssl-dev \
    readline-dev \
  && ./configure \
  && make \
  && make install \
  && rm -rf /build \
  && apk del .build-deps \
  && apk add --no-cache --virtual .persistent-deps \
    ncurses \
    openssl \
    readline \
    iptables \
  && chmod +x /usr/local/bin/generate \
  && chmod +x /usr/local/bin/vpnserver-start

VOLUME ["/usr/vpnserver/server_log/"]

CMD ["vpnserver-start"]

EXPOSE 500/udp 4500/udp 1701/tcp 1194/udp 5555/tcp 443/tcp